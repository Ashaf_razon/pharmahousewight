import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class UserHome extends StatelessWidget{
  final TextEditingController weightController = TextEditingController();
  final databaseReference = FirebaseFirestore.instance;
  //List<String> wItem = [];

  final Stream<QuerySnapshot> users =
      FirebaseFirestore.instance.collection('usersweight').snapshots();
  var globalHistory = "".obs;

  getData() async{
    try{
      globalHistory.value = "";
      //wItem = ["Weight-Date time list"];
      await databaseReference.collection("usersweight").orderBy("date",descending: true).get().then((querySnapshot) {
        querySnapshot.docs.forEach((result) {
          String serverData = "Date: "+result['date'].toString()+" ""Weight: "+result['weight'].toString();
          globalHistory.value += serverData+"\n\n";
          //wItem.add(serverData);
          print(serverData);
        });
      });
    }catch(e){
      Get.defaultDialog(title: "Alert!", middleText: "Sorry! data retrieving failed"+e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: //Text('Login/ Sign up page'),
            Text('Home Page'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration:
                        const InputDecoration(hintText: "Enter your weight"),
                        controller: weightController,
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ElevatedButton(
                              child: Text('Add weight'),
                              onPressed: () {
                                createRecord(weightController.text);
                              }),
                          ElevatedButton(child: Text('View History'), onPressed: () {
                            getData();
                          }),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Container(
                  alignment: Alignment.bottomLeft,
                  child: Obx(()=> Text('$globalHistory')),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void createRecord(String weight) async {
    try {
      int weightUser = int.parse(weight);
      if (weightUser > 0) {
        DateTime now = DateTime.now();
        String formattedDate = DateFormat('kk:mm:ss \n EEE d MMM').format(now);
        DocumentReference ref =
            await databaseReference.collection("usersweight").add({
          'date': formattedDate.toString(),
          'weight': weightUser,
        });
        print("" + ref.id.toString());
        Get.defaultDialog(title: "Congratulations!", middleText: "Save success");
      }
    } catch (e) {
      Get.defaultDialog(title: "Alert!", middleText: "Save failed"+e.toString());
    }
  }

}

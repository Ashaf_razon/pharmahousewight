import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'homepage.dart';
import 'package:intl/intl.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyWeightTracker());
}

class MyWeightTracker extends StatefulWidget {
  const MyWeightTracker({Key? key}) : super(key: key);

  @override
  _AuthAppState createState() => _AuthAppState();
}

class _AuthAppState extends State<MyWeightTracker> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: //Text('Login/ Sign up page'),
              Text('Auth User (Logged ' + (user == null ? 'out' : 'in') + ')'),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  decoration:
                      const InputDecoration(hintText: "Enter your e-mail"),
                  controller: emailController,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  decoration:
                      const InputDecoration(hintText: "Enter your password"),
                  controller: passwordController,
                  obscureText: true,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                        child: Text('Sign Up'),
                        onPressed: () {
                          userSignUp(
                              emailController.text, emailController.text, user);
                        }),
                    ElevatedButton(
                        child: Text('Sign In'),
                        onPressed: () {
                          userSignIn(
                              emailController.text, emailController.text, user);
                        }),
                    ElevatedButton(
                        child: Text('Log Out'),
                        onPressed: () {
                          userSignOut();
                        }),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void userSignIn(String email, String password, User? user) async {
    try {
      if (email != null && password.length >= 6) {
        await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: password,
        );
        //print("User sIn: "+user.toString());
        if (FirebaseAuth.instance.currentUser != null) {
          Get.to(UserHome());
        } else {
          Get.defaultDialog(
              title: "Alert!", middleText: "Login failed");
        }
      }

      setState(() {});
    } catch (e) {
      Get.snackbar("Error Sign In User", e.toString(),
          snackPosition: SnackPosition.BOTTOM);
    }
  }

  void userSignUp(String email, String password, User? user) async {
    try {
      if (email != null && password.length >= 6) {
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password,
        );
        //print("User sUp: "+user.toString());
        //Get.to(UserHome());
        DateTime now = DateTime.now();
        String formattedDate = DateFormat('kk:mm:ss \n EEE d MMM').format(now);
        Get.defaultDialog(
            title: "Welcome "+formattedDate, middleText: "You are successfully signed up");

        setState(() {});
      } else {
        Get.defaultDialog(
            title: "Alert!", middleText: "Field must be filled up.");
      }
    } catch (e) {
      Get.snackbar("Error Sign up User", e.toString(),
          snackPosition: SnackPosition.BOTTOM);
    }
  }

  void userSignOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      setState(() {});
    } catch (e) {
      Get.snackbar("Error Sign Out User", e.toString(),
          snackPosition: SnackPosition.BOTTOM);
    }
  }
}
